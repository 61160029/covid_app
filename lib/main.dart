import 'package:covid_app/profile.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Covid 19',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Main')),
      body: ListView(
        children: [
          ListTile(
            title: Text('Profile'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
            },
          ),
          ListTile(
            title: Text('Vaccine'),
            onTap: () {},
          )
        ],
      ),
    );
  }
}
